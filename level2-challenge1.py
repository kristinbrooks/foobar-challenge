# Don't Get Volunteered!
# ======================
#
# As a henchman on Commander Lambda's space station, you're expected to be resourceful, smart, and
# a quick thinker. It's not easy building a doomsday device and ordering the bunnies around at the
# same time, after all! In order to make sure that everyone is sufficiently quick-witted, Commander
# Lambda has installed new flooring outside the henchman dormitories. It looks like a chessboard,
# and every morning and evening you have to solve a new movement puzzle in order to cross the floor.
# That would be fine if you got to be the rook or the queen, but instead, you have to be the knight.
# Worse, if you take too much time solving the puzzle, you get "volunteered" as a test subject for
# the LAMBCHOP doomsday device!
#
# To help yourself get to and from your bunk every day, write a function called solution(src, dest)
# which takes in two parameters: the source square, on which you start, and the destination square,
# which is where you need to land to solve the puzzle.  The function should return an integer
# representing the smallest number of moves it will take for you to travel from the source square
# to the destination square using a chess knight's moves (that is, two squares in any direction
# immediately followed by one square perpendicular to that direction, or vice versa, in an "L"
# shape).  Both the source and destination squares will be an integer between 0 and 63, inclusive,
# and are numbered like the example chessboard below:
#
# -------------------------
# | 0| 1| 2| 3| 4| 5| 6| 7|
# -------------------------
# | 8| 9|10|11|12|13|14|15|
# -------------------------
# |16|17|18|19|20|21|22|23|
# -------------------------
# |24|25|26|27|28|29|30|31|
# -------------------------
# |32|33|34|35|36|37|38|39|
# -------------------------
# |40|41|42|43|44|45|46|47|
# -------------------------
# |48|49|50|51|52|53|54|55|
# -------------------------
# |56|57|58|59|60|61|62|63|
# -------------------------
#
# Test cases
# ==========
# Your code should pass the following test cases.
# Note that it may also be run against hidden test cases not shown here.
#
# -- Python cases --
# Input:
# solution.solution(0, 1)
# Output:
#     3
#
# Input:
# solution.solution(19, 36)
# Output:
#     1

# Submitted solution
from collections import deque


def solution(src, dest):
    if src == dest:
        return 0
    chessboard = [[(8 * y) + x for x in range(8)] for y in range(8)]
    src_position = find_coordinate_position(chessboard, src)
    (src_x, src_y) = src_position
    dest_position = find_coordinate_position(chessboard, dest)
    num_moves = 0
    visited_squares = [(src_x, src_y, num_moves)]
    moves = deque()
    moves.append((src_x, src_y, num_moves))
    found_dest = False
    while not found_dest:
        dequeued_item = moves.popleft()
        valid_moves = get_valid_moves(dequeued_item)
        for move in valid_moves:
            (move_x, move_y, num_moves) = move
            if (move_x, move_y) == dest_position:
                found_dest = True
                return num_moves
            if move not in visited_squares:
                visited_squares.append(move)
                moves.append(move)


def find_coordinate_position(board, src):
    row = src // 8
    col = src % 8
    return col, row


def get_valid_moves((x, y, num_moves)):
    possible_moves = [(2, -1), (2, 1), (-2, -1), (-2, 1), (1, -2), (1, 2), (-1, -2), (-1, 2)]
    valid_moves = []
    for (col, row) in possible_moves:
        move = x + col, y + row, num_moves + 1
        if is_valid_move(move):
            valid_moves.append(move)
    return valid_moves


def is_valid_move((x, y, num_moves)):
    return 0 <= x <= 7 and 0 <= y <= 7


# Known test cases
print(solution(0, 1))
print(solution(19, 36))
