# The Grandest Staircase Of Them All
# ==================================
#
# With the LAMBCHOP doomsday device finished, Commander Lambda is preparing to debut on the
# galactic stage -- but in order to make a grand entrance, Lambda needs a grand staircase! As the
# Commander's personal assistant, you've been tasked with figuring out how to build the best
# staircase EVER.
#
# Lambda has given you an overview of the types of bricks available, plus a budget. You can buy
# different amounts of the different types of bricks (for example, 3 little pink bricks, or 5 blue
# lace bricks). Commander Lambda wants to know how many different types of staircases can be built
# with each amount of bricks, so they can pick the one with the most options.
#
# Each type of staircase should consist of 2 or more steps.  No two steps are allowed to be at the
# same height - each step must be lower than the previous one. All steps must contain at least one
# brick. A step's height is classified as the total amount of bricks that make up that step.
# For example, when N = 3, you have only 1 choice of how to build the staircase, with the first
# step having a height of 2 and the second step having a height of 1: (# indicates a brick)
#
#  #
#  ##
#  21
#
# When N = 4, you still only have 1 staircase choice:
#
#  #
#  #
#  ##
#  31
#
# But when N = 5, there are two ways you can build a staircase from the given bricks. The two
# staircases can have heights (4, 1) or (3, 2), as shown below:
#
#  #
#  #
#  #
#  ##
#  41
#
#  #
#  ##
#  ##
#  32
#
# Write a function called solution(n) that takes a positive integer n and returns the number of
# different staircases that can be built from exactly n bricks. n will always be at least 3 (so you
# can have a staircase at all), but no more than 200, because Commander Lambda's not made of money!
#
# Test cases
# ==========
# Your code should pass the following test cases.
# Note that it may also be run against hidden test cases not shown here.
#
# -- Python cases --
# Input:
# solution.solution(200)
# Output:
#     487067745
#
# Input:
# solution.solution(3)
# Output:
#     1

# Submitted solution
def solution(n):
    staircases = []
    out = [None] * n
    print_combinations(1, n, out, 0, staircases)
    return len(staircases)


# I understand what I wasn't getting from my previous approach, but am not quite sure how to fix it.
# I am temporarily borrowing the following function from
# https://www.techiedelight.com/print-all-combination-numbers-from-1-to-n/
# to test and understand how it works differently from the approach I was doing.
# Then I can start again, because I will figure this challenge out. :)
def print_combinations(i, n, out, index, staircases):
    # if the sum becomes `n`, print the combination
    if n == 0 and check_solution(index, out, staircases):
        staircases.append(out[:index])

    # start from the previous element in the combination till `n`
    for j in range(i, n + 1):
        # place current element at the current index
        out[index] = j

        # recur with a reduced sum
        print_combinations(j, n - j, out, index + 1, staircases)


def check_solution(index, out, staircases):
    current_solution = out[:index]
    return 1 < len(current_solution) == len(
        set(current_solution)) and current_solution not in staircases


# Known test cases
# print(solution(200))
# print(solution(3))
# print('n: 3')
# print('number of solutions: ' + str(solution(3)))
# print('')
# print('n: 4')
# print('number of solutions: ' + str(solution(4)))
# print('')
# print('n: 5')
# print('number of solutions: ' + str(solution(5)))
# print('')
# print('n: 6')
# print('number of solutions: ' + str(solution(6)))
# print('')
# print('n: 7')
# print('number of solutions: ' + str(solution(7)))
# print('')
# print('n: 8')
# print('number of solutions: ' + str(solution(8)))
# print('')
# print('n: 9')
# print('number of solutions: ' + str(solution(9)))
# print('')
# print('n: 10')
# print('number of solutions: ' + str(solution(10)))
# print('')
# print('n: 20')
# print('number of solutions: ' + str(solution(20)))
# print('')
# print('n: 50')
# print('number of solutions: ' + str(solution(50)))
# print('')
# print('n: 75')
# print('number of solutions: ' + str(solution(75)))
# print('')
print('n: 100')
print('number of solutions: ' + str(solution(100)))
print('')
# print('n: 200')
# print('number of solutions: ' + str(solution(200)))
# print('')
