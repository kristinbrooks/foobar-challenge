# Please Pass the Coded Messages
# ==============================
#
# You need to pass a message to the bunny workers, but to avoid detection, the code you agreed to
# use is... obscure, to say the least. The bunnies are given food on standard-issue plates that are
# stamped with the numbers 0-9 for easier sorting, and you need to combine sets of plates to create
# the numbers in the code. The signal that a number is part of the code is that it is divisible by
# 3. You can do smaller numbers like 15 and 45 easily, but bigger numbers like 144 and 414 are a
# little trickier. Write a program to help yourself quickly create large numbers for use in the
# code, given a limited number of plates to work with.
#
# You have L, a list containing some digits (0 to 9). Write a function solution(L) which finds the
# largest number that can be made from some or all of these digits and is divisible by 3. If it is
# not possible to make such a number, return 0 as the solution. L will contain anywhere from 1 to
# 9 digits.  The same digit may appear multiple times in the list, but each element in the list may
# only be used once.
#
# Test cases
# ==========
# Your code should pass the following test cases.
# Note that it may also be run against hidden test cases not shown here.
#
# -- Python cases --
# Input:
# solution.solution([3, 1, 4, 1])
# Output:
#     4311
#
# Input:
# solution.solution([3, 1, 4, 1, 5, 9])
# Output:
#     94311

# Submitted solution
import itertools


def solution(l):
    # the numbers on list l need to be strings to use the join function later
    l_str = [str(num) for num in l]
    combinations = find_combinations(l_str)
    permutations = find_permutations_of_list_of_combinations(combinations)
    possible_numbers = convert_string_tuples_to_ints(permutations)
    return find_largest_divisible_by_three(possible_numbers)


def convert_string_tuples_to_ints(tup_list):
    int_nums = []
    for tup in tup_list:
        num = int(''.join(tup))
        int_nums.append(num)
    return int_nums


def find_combinations(num_list):
    combinations = []
    for i in range(1, len(num_list) + 1, 1):
        combinations.extend(list(itertools.combinations(num_list, i)))
    return combinations


def find_permutations_of_list_of_combinations(combination_list):
    permutations = []
    for combination in combination_list:
        permutations.extend(list(itertools.permutations(combination)))
    return permutations


def find_largest_divisible_by_three(num_list):
    largest_num = 0
    for num in num_list:
        if num % 3 == 0 and num > largest_num:
            largest_num = num
    return largest_num


# Known test cases
print(solution([3, 1, 4, 1]))
print(solution([3, 1, 4, 1, 5, 9]))
