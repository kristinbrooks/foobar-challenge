# Google Foobar Challenge

The foobar challenge is an invitation only hiring challenge created by Google. You can either get a 
direct invitation from Google based on your search history or via an invitation link from someone 
else who has completed at least three levels of the challenge. The challenge has 5 levels of 
increasing difficulty. 

I had never heard of the foobar challenge before when suddenly this happened to my screen:

![surprise foobar challenge invite](images/surprise_invite.png)

I clicked 'I want to play' and this opened:

![opening screen of foobar challenge](images/accepted_invite.png)

This repo contains challenge info the code I submitted for the version of the challenge I took 
part in.

## Journal

These are the entries automatically added to journal.txt by Google as you procede throught the 
challenge.

> Success! You've managed to infiltrate Commander Lambda's evil organization, and finally earned yourself an entry-level position as a Minion on their space station. From here, you just might be able to subvert Commander Lambda's plans to use the LAMBCHOP doomsday device to destroy Bunny Planet. Problem is, Minions are the lowest of the low in the Lambda hierarchy. Better buck up and get working, or you'll never make it to the top...

Added after level 1 challenge request:

> Next time Bunny HQ needs someone to infiltrate a space station to rescue bunny workers, you're going to tell them to make sure 'stay up for 48 hours straight scrubbing toilets' is part of the job description. It's only fair to warn people, after all.

Added after level 1 submission:

> You survived a week in Commander Lambda's organization, and you even managed to get yourself promoted. Hooray! Henchmen still don't have the kind of security access you'll need to take down Commander Lambda, though, so you'd better keep working. Chop chop!

Added after level 2, challenge 1 request:

> At least all this time spent running errands all over Commander Lambda's space station have given you a really good understanding of the station's layout. You'll need that when you're finally ready to destroy the LAMBCHOP and rescue the bunny workers.

Added after level 2, challenge 1 submission:

> You got the bunny trainers to teach you a card game today, it's called Fizzbin. It's kind of pointless, but they seem to like it and it helps you pass the time while you work your way up to Commander Lambda's inner circle.

Added after level 2, challenge 2 submission:

> Awesome! Commander Lambda was so impressed by your efforts that you've been promoted to personal assistant. You'll be helping the Commander directly, which means you'll have access to all of Lambda's files -- including the ones on the LAMBCHOP doomsday device. This is the chance you've been waiting for. Can you use your new access to finally topple Commander Lambda's evil empire?

Added after requested level 3, challenge 1 request:

> There are a lot of difficult things about being undercover as Commander Lambda's personal assistant, but you have to say, the personal spa and private hot cocoa bar are pretty awesome.

## Python Constraints

Your code will run inside a Python 2.7.13 sandbox. All tests will be run by calling the solution()
function.

Standard libraries are supported except for bz2, crypt, fcntl, mmap, pwd, pyexpat, select, signal,
termios, thread, time, unicodedata, zipimport, zlib.

Input/output operations are not allowed.

Your solution must be under 32000 characters in length including new lines and other
non-printing characters.

## Shell Commands

`cd`	change directory [dir_name]

`cat`	print file [file_name]

`deleteme`	delete all of your data associated with foobar

`edit`	open file in editor [file_name]

`feedback`	provide feedback on foobar

`less`	print a file a page at a time [file_name]

`ls`	list directory contents [dir_name]

`request`	request new challenge

`status`	print progress

`submit`	submit final solution file for assessment [file_name]

`verify`	runs tests on solution file [file_name]

Keyboard help:

`⌘ + S`	save the open file [when editor is focused]

`⌘ + E`	close the editor [when editor is focused]

Toggle between the editor and terminal using `ESC` followed by `TAB`, then activate with `ENTER`.
